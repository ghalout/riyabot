


MessageTemplates={

    "simple":{
        "createMessageGreeting":{
            "basic":[
                "Hey I am Ritesh ! "
            ],

            "namePresent": [
                "Thank you! Bye!",

            ],
            "askForUserForm": [
                "Awesome, please fill in your details and we will get back to you soonest.",

            ],
            "CatPresent": [
                "I can help to plan your tour.<br> What are you looking for today?"
            ],
            "catSelected":[
             " Awesome! <br> It's great that you have selected <b>{0}</b>.",
                "Great! You selected <b>{0}</b>."
            ],
            "selectRegions": [
                "In which month you are planning to travel?"

            ],
            "durationSubCatPresent": [
                "How long you want the tour to be?"
            ],
             "awsome":[
                 "Awesome!"
             ],
             "about":[
                 "I am a <b>travel expert</b> from <b>Riya Holidays</b>.",

             ],
            "aboutIndia": [
                "India's culture is among the world's oldest; civilization in India began about 4,500 years ago.",

            ],
            "aboutIndia2": [
                " Facts about international travel are:",

            ],
            "aboutIndia3": [
                "1) The number of Indians travelling abroad has gone up 2.5 times in a decade.<br> 2)The UNWTO predicts that India will account for 50 million outbound tourists by 2020",

            ],
            "bestPlace":["Every state has it's own importance. Based on our knowledge I'll recommend you the best state for travel."],
            "bestCountry":["I'll recommend you the best destination according to weather."],
}
    }
}


