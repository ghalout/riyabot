# -*- coding: utf-8 -*-
# -*- coding: utf-8 -*-
from __future__ import unicode_literals



import logging
from django.conf import settings

from PaxcelBotFramework.BotManager.templateBot import TemplateBot
logger = logging.getLevelName(__name__)
# Create your models here.

from botTemplates import TemplateBotData as botTemplates

def getTemplateBotResponse(botRequest):
        """

        :param botRequest:
        :return:
        """
        templateBot = TemplateBot(botTemplate=botTemplates[settings.TEMPLATE_BOT])

        botResponse = templateBot.getResponse(botRequest)


        return botResponse